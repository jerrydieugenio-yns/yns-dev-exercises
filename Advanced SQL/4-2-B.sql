CREATE DATABASE IF NOT EXISTS `yns`;

USE `yns`;

CREATE TABLE IF NOT EXISTS therapists (
id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS daily_work_shifts (
id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
therapist_id INT(11) NOT NULL,
target_date DATE NOT NULL,
start_time TIME NOT NULL,
end_time TIME NOT NULL
);

INSERT INTO therapists(name)
VALUES('John'),
('Arnold'),
('Robert'),
('Ervin'),
('Smith');



-- Excercise 4-2-B Starts Here

INSERT INTO daily_work_shifts(therapist_id, target_date, start_time,end_time)
VALUES(1, NOW(), '14:00:00','15:00:00'),
(2, NOW(), '22:00:00','23:00:00'),
(3, NOW(), '00:00:00','01:00:00'),
(4, NOW(), '05:00:00','05:30:00'),
(1,NOW(), '20:00:00','20:45:00'),
(5, NOW(), '05:30:00','05:50:00'),
(3,NOW(), '02:00:00','02:30:00');