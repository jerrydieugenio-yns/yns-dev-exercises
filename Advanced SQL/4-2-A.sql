CREATE DATABASE IF NOT EXISTS `yns`;

USE `yns`;

CREATE TABLE IF NOT EXISTS therapists (
id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS daily_work_shifts (
id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
therapist_id INT(11) NOT NULL,
target_date DATE NOT NULL,
start_time TIME NOT NULL,
end_time TIME NOT NULL
);

INSERT INTO therapists(name)
VALUES('John'),
('Arnold'),
('Robert'),
('Ervin'),
('Smith');