SELECT E.*,
(CASE 
    WHEN P.name = "CEO" THEN "Chief Executive Officer"
    WHEN  P.name = "CTO" THEN "Chief Technical Officer"
    WHEN  P.name = "CFO" THEN "Chief Financial Officer"
    ELSE P.name
END)
AS full_position_name
FROM employee_positions EP
INNER JOIN
positions P
ON (EP.position_id = P.id)
INNER JOIN
employees E
ON (EP.employee_id = E.id);