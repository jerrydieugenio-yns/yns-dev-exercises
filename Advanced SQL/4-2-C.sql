SELECT DWS.therapist_id, DWS.target_date, DWS.start_time, DWS.end_time, 
(CASE 
    WHEN DWS.start_time <= '05:59:59' AND DWS.start_time >= '00:00:00' 
    THEN CONCAT(DATE_ADD(DWS.target_date, INTERVAL 1 DAY),' ', DWS.start_time) 
    ELSE CONCAT(DWS.target_date ,' ', DWS.start_time) 
END) 
AS sort_start_time 
FROM daily_work_shifts DWS ORDER BY target_date, sort_start_time ASC;