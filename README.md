# The capacity to learn is a **gift**; the ability to learn is a **skill**; the willingness to learn is a **choice** - Brian Herbert

### Welcome to YNS Dev Excercises

### Developer: **JD**

> #### Thank you and I am honored and desperately in need of your honest and open feedback.

&nbsp;
&nbsp;

![Your Feedback Matters](https://diamondsrock.com/wp-content/uploads/2018/08/your-feedback-matters.png)
