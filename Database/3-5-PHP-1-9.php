<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>1-9. User Information - Result</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <style>
    body {
      width: 100vw;
      overflow-x: hidden;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    .container {
      width: 80%;
      margin: 0 auto;
      padding: 20px 40px;
      border: 1px solid #000;
      border-radius: 10px;
      background-color: #1a1a1a;
      color: #fff;
      box-shadow: 3px 3px 5px 6px #ccc;
    }

    .container h1 {
      color: yellow;
    }

    table {
      width: 100%;
      background: rgba(255, 255, 255, 0.05);
      box-shadow: 0 8px 32px 0 rgba(31, 38, 135, 0.37);
      backdrop-filter: blur(9.0px);
      -webkit-backdrop-filter: blur(9.0px);
      border-radius: 2px;
      border: 1px solid rgba(255, 255, 255, 0.18);
    }

    td,
    th {
      border: 1px solid #999;
      padding: 0.5rem;
    }
  </style>
</head>

<body>

  <div class="container">
    <h1>User Information List</h1>
    <table>
      <thead>
        <tr>
          <th>First Name</th>
          <th>Middle Name</th>
          <th>Last Name</th>
          <th>Email</th>
          <th>Date of Birth</th>
          <th>Age</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $server = "127.0.0.1";
        $username = "root";
        $password = "";
        $database = "yns_dev_exercises";
        $con = new mysqli($server, $username, $password, $database);
        if (mysqli_connect_error()) {
          trigger_error("Failed to connect to Database: " . mysqli_connect_error());
        }

        //Fetch All Data from Users Table
        $stmt = $con->prepare("SELECT * FROM users");
        $stmt->execute();
        $users = $stmt->get_result();

        $stmt->execute();

        if ($users->num_rows > 0) {
          foreach ($users as $user) {
            $dateOfBirth = $user['dateOfBirth'];
            $age = intval(date('Y-m-d')) - intval($dateOfBirth);
        ?>
            <tr>
              <td> <?= $user['firstName'] ?> </td>
              <td> <?= $user['middleName'] ?> </td>
              <td> <?= $user['lastName'] ?> </td>
              <td> <?= $user['email']; ?> </td>
              <td> <?= $dateOfBirth ?> </td>
              <td> <?= $age ?> </td>
            </tr>
        <?php
          }
        }

        ?>
      </tbody>
    </table>

  </div>
</body>

</html>