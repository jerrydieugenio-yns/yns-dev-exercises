SELECT E.first_name, E.middle_name, E.last_name 
FROM `employee_positions` EP
JOIN employees E 
ON (EP.employee_id = E.id)
HAVING COUNT(EP.employee_id) > 2;