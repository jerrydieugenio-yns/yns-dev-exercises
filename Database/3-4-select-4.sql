SELECT 
Employee.last_name as Employee,
Boss.last_name as Boss
FROM `employees` Employee
JOIN `employees` Boss
ON (Employee.boss_id = Boss.id);