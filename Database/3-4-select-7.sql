SELECT `departments`.name, 
COUNT(`departments`.id) 
AS 'Number Of Employee' 
FROM departments 
JOIN employees  
ON `departments`.id = `employees`.department_id 
GROUP BY `departments`.id;