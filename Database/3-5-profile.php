<?php
include '3-5-api.php';
session_start();

if (!isset($_SESSION['user'])) {
    header('Location: 3-5-login.php');
}
$users = new Users();
if (isset($_POST['logout'])) {
    $users->logout();
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home - User Information App</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
        body {
            height: 100%;
            width: 100vw;
            overflow-x: hidden;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .container {
            width: 80%;
            margin: 0 auto;
            padding: 20px 40px;
            border: 1px solid #000;
            border-radius: 10px;
            background-color: #1a1a1a;
            color: #fff;
            box-shadow: 3px 3px 5px 6px #ccc;
            text-align: center;
        }

        .container h1 {
            color: yellow;

        }

        .profile-container {
            width: 60%;
            margin: 0 auto;
        }

        .profile-container h1 {
            font-size: 50px;
        }

        .profile-container>* {
            margin: 10px 0;
        }

        .profile-container p {
            font-size: 30px;
        }


        div.center {
            text-align: center;
        }

        ul.breadcrumb {
            padding: 10px 16px;
            list-style: none;
            transition: background-color .3s;
            background: rgba(255, 255, 255, 0.08);
            box-shadow: 0 8px 32px 0 rgba(31, 38, 135, 0.37);
            backdrop-filter: blur(9.0px);
            -webkit-backdrop-filter: blur(9.0px);
            border-radius: 2px;
            border: 1px solid rgba(255, 255, 255, 0.18);

        }

        ul.breadcrumb li {
            display: inline;
            font-size: 18px;
        }

        ul.breadcrumb li+li:before {
            padding: 8px;
            color: black;
            content: "/\00a0";
        }

        ul.breadcrumb li a {
            color: #A9A9A9;
            text-decoration: none;
        }

        ul.breadcrumb li a:hover {
            color: #fff;
            text-decoration: underline;
        }

        ul.breadcrumb li a.active {
            color: #fff;
        }

        .text-center {
            text-align: center;
        }


        .hidden {
            display: none;
        }

        .profile {
            width: 200px;
            height: 200px;
            border-radius: 50%;
            object-fit: cover;
            background-color: #fff;
        }
    </style>
</head>

<body>

    <div class="container">
        <h1>User Information App</h1>
        <ul class="breadcrumb">
            <li><a href="3-5-index.php">Home</a></li>
            <li><a class="active" href="3-5-create.php">Create User</a></li>
            <li>
                <a href="3-5-profile.php">
                    <?= (isset($_SESSION['name'])) ? $_SESSION['name'] : '' ?>

                </a>
            </li>
            <li>
                <a href="#" onclick="document.querySelector('#logout').click()"> <i class="fas fa-power-off"></i> Logout</a>
            </li>
            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="logoutForm">
                <li>
                    <!-- form elements -->
                    <input type="submit" name="logout" id="logout" value="Logout" class="hidden">
                </li>
            </form>
        </ul>



        <?php
        $users = $users->fetchOneById($_SESSION['id']);
        foreach ($users as $user) {
            $fullName = $user['firstName'] . ' ' . $user['middleName'] . ' ' . $user['lastName'];
            $dateOfBirth = $user['dateOfBirth'];
            $age = intval(date('Y-m-d')) - intval($dateOfBirth);
        ?>
            <div class="profile-container">
                <img src="uploads/<?= $user['image']; ?>" alt="Profile Image" class="profile">
                <h1>
                    <?= $fullName ?>
                </h1>
                <p><?= $user['email']; ?> </p>
                <p>username: <?= $user['username']; ?> </p>
                <p> <?= $dateOfBirth ?> </p>
                <p><?= $age ?> years old.</p>

            </div>
        <?php } ?>


    </div>
</body>

</html>