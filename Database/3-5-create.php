<?php
include '3-5-api.php';
session_start();

$users = new Users();
if (isset($_POST['create'])) {
  $created = $users->create($_POST);
}

if (isset($_POST['logout'])) {
  $users->logout();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Create User - User Information App</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />

  <style>
    body {
      height: 100%;
      width: 100vw;
      overflow-x: hidden;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    .container {
      width: 40%;
      margin: 0 auto;
      padding: 20px 40px;
      border: 1px solid #000;
      border-radius: 10px;
      background-color: #1a1a1a;
      color: #fff;
      box-shadow: 3px 3px 5px 6px #ccc;
      display: flex;
      flex-direction: column;
      justify-content: center;
    }

    .container div {
      display: flex;
      flex-direction: column;
      justify-content: center;
    }

    .container h1 {
      color: rgba(238, 238, 0, 1);
    }

    input[type=text],
    input[type=date],
    input[type=email],
    input[type=password],
    input[type=file] {
      padding: 10px 15px;
      margin: 10px auto;
      width: 80%;
    }

    label {
      margin: 0 40px;
      width: 80%;
    }

    .button-container {
      padding: 10px 20px;
      margin: 10px auto;
      width: 50%;
    }

    input[type=submit] {
      padding: 10px 20px;
      margin: 10px auto;
      width: 80%;
      border: none;
      box-shadow: 2px 1px 1px #ccc;
    }

    input[type=submit]:hover {
      background-color: rgba(238, 238, 0, 1);
    }

    .errors {
      background-color: rgba(255, 51, 51, .7);
      padding: 20px;
      margin: 10px 35px;
      border: 1px solid #FF3333;
      font-size: 14px;
      display: flex;
      flex-direction: row;
      justify-content: space-between;
    }

    .success-message {

      background-color: #4BB543;
      padding: 20px;
      margin: 10px 35px;
      border: 1px solid #4BB5;
      font-size: 14px;
      display: flex;
      flex-direction: row;
      justify-content: space-between;

    }


    ul.breadcrumb {
      padding: 10px 16px;
      list-style: none;
      transition: background-color .3s;
      background: rgba(255, 255, 255, 0.08);
      box-shadow: 0 8px 32px 0 rgba(31, 38, 135, 0.37);
      backdrop-filter: blur(9.0px);
      -webkit-backdrop-filter: blur(9.0px);
      border-radius: 2px;
      border: 1px solid rgba(255, 255, 255, 0.18);

    }

    ul.breadcrumb li {
      display: inline;
      font-size: 18px;
    }

    ul.breadcrumb li+li:before {
      padding: 8px;
      color: black;
      content: "/\00a0";
    }

    ul.breadcrumb li a {
      color: #A9A9A9;
      text-decoration: none;
    }

    ul.breadcrumb li a:hover {
      color: #fff;
      text-decoration: underline;
    }

    ul.breadcrumb li a.active {
      color: #fff;
    }

    .hidden {
      display: none;
    }
  </style>
</head>

<body>
  <div class="container">
    <h1>User Information</h1>
    <ul class="breadcrumb">

      <?= (isset($_SESSION['user'])) ? '<li><a href="3-5-index.php">Home</a></li>' : '' ?>

      <li>
        <a class="active" href="3-5-create.php">
          <?= (isset($_SESSION['user'])) ? 'Create User' : 'Sign Up' ?>
        </a>
      </li>
      <li>
        <a href="3-5-profile.php">
          <?= (isset($_SESSION['name'])) ? $_SESSION['name'] : '' ?>
        </a>
      </li>
      <?php
      if (isset($_SESSION['user'])) {
      ?>
        <li>
          <a href="#" onclick="document.querySelector('#logout').click()"> <i class="fas fa-power-off"></i> Logout</a>
        </li>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="logoutForm">
          <li>
            <!-- form elements -->
            <input type="submit" name="logout" id="logout" value="Logout" class="hidden">
          </li>
        </form>
      <?php
      } else {
        echo '<li><a href="3-5-login.php">Already have an account?</a></li>';
      }
      ?>


    </ul>

    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">

      <?php
      $icon = '<i class="fas fa-exclamation-triangle"></i>';

      if (isset($created["errors"])) {
        $errors = $created["errors"];
        echo '<div class="errors">';
        foreach ($errors as $error) {
          echo $icon . $error;
        }
        echo '</div>';
      }
      ?>
      <?= isset($created["success"]) ? '<h5 class="success-message">' . $created["success"] . '</h5>
      <label>Given Name: </label>' : '';  ?>

      <div>
        <input type="text" name="firstName" placeholder="Given name" value="<?= (isset($created["data"]) && !isset($created["success"])) ? $created["data"]["firstName"] : '' ?>" />
      </div>
      <label>Middle Name: </label>
      <div>
        <input type="text" name="middleName" placeholder="Middle name" value="<?= (isset($created["data"]) && !isset($created["success"])) ? $created["data"]["middleName"] : '' ?>" />
      </div>
      <label>Surname Name: </label>
      <div>
        <input type="text" name="lastName" placeholder="Sur name" value="<?= (isset($created["data"]) && !isset($created["success"])) ? $created["data"]["lastName"] : '' ?>" />
      </div>
      <label>Email: </label>
      <div>
        <input type="text" name="email" placeholder="Email Address" value="<?= (isset($created["data"]) && !isset($created["success"])) ? $created["data"]["email"] : '' ?>" />
      </div>
      <label>Username: </label>
      <div>
        <input type="text" name="username" placeholder="Username" value="<?= (isset($created["data"]) && !isset($created["success"])) ? $created["data"]["username"] : '' ?>" />
      </div>
      <label>Password: </label>
      <div>
        <input type="password" name="password" placeholder="Password" value="" />
      </div>
      <label>Birth date: </label>
      <div>
        <input type="date" name="dateOfBirth" value="<?= (isset($created["data"]) && !isset($created["success"])) ? $created["data"]["dateOfBirth"] : '' ?>" />
      </div>
      <div>
        <label for="">Profile:</label>
        <input type="file" name="fileToUpload" id="fileToUpload" value="<?= (isset($created["data"]) && !isset($created["success"])) ? $created["data"]["image"] : '' ?>" />
      </div>
      <div class="button-container">
        <input type="submit" name="create">
      </div>
    </form>
  </div>
</body>

</html>