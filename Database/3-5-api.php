<?php
class Users
{
  private $server = "127.0.0.1";
  private $username = "root";
  private $password = "";
  private $database = "yns_dev_exercises";
  public $con;

  public function __construct()
  {
    $this->con = new mysqli($this->server, $this->username, $this->password, $this->database);
    if (mysqli_connect_error()) {
      trigger_error("Failed to connect to Database: " . mysqli_connect_error());
    } else {
      return $this->con;
    }
  }

  public function index()
  {
    //Fetch All Data from Users Table
    $stmt = $this->con->prepare("SELECT * FROM users");
    $stmt->execute();
    $result = $stmt->get_result();
    $data = array();
    if ($result->num_rows > 0) {
      while ($row = $result->fetch_assoc()) {
        $data[] = $row;
      }
    }
    return $data;
  }
  public function authenticate($post)
  {
    $errors = array();
    $username = $_POST["username"];
    // $password = $_POST["password"];

    // Prepare our SQL, preparing the SQL statement will prevent SQL injection.
    if ($stmt = $this->con->prepare('SELECT id, password FROM users WHERE username = ?')) {
      // Bind parameters (s = string, i = int, b = blob, etc), in our case the username is a string so we use "s"
      $stmt->bind_param('s', $username);
      $stmt->execute();
      // Store the result so we can check if the account exists in the database.
      $stmt->store_result();

      if ($stmt->num_rows > 0) {
        $stmt->bind_result($id, $password);
        $stmt->fetch();
        // Account exists, now we verify the password.
        // Note: remember to use password_hash in your registration file to store the hashed passwords.
        if (password_verify($_POST['password'], $password)) {
          // Verification success! User has logged-in!
          // Create sessions, so we know the user is logged in, they basically act like cookies but remember the data on the server.
          session_regenerate_id();
          $_SESSION['loggedin'] = TRUE;
          $_SESSION['user'] = $username;
          $_SESSION['id'] = $id;
          // echo 'Welcome ' . $_SESSION['user'] . '!';
          header('Location: 3-5-index.php');
        } else {
          // Incorrect password
          array_push($errors, "Incorrect username and/or password!");
        }
      } else {
        // Incorrect username
        array_push($errors, "Incorrect username and/or password!");
      }


      $stmt->close();
      if (count($errors) > 0) {
        return ["errors" => $errors];
      }
    }
  }

  public function create()
  {
    $firstName = $this->con->real_escape_string($_POST['firstName']);
    $lastName = $this->con->real_escape_string($_POST['lastName']);
    $middleName = $this->con->real_escape_string($_POST['middleName']);
    $email = $this->con->real_escape_string($_POST['email']);
    $username = $this->con->real_escape_string($_POST['username']);
    $password = $this->con->real_escape_string($_POST['password']);
    $dateOfBirth = $this->con->real_escape_string($_POST['dateOfBirth']);
    $errors = array();
    $age = intval(date('Y-m-d')) - intval($dateOfBirth);

    function validateStringInput($input)
    {
      return preg_match("/^[a-zA-Z\s]+$/", $input);
    }
    function validateEmail($email)
    {
      $pattern = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i";
      return preg_match($pattern, $email);
    }
    function validatePassword($password)
    {
      $uppercase = preg_match('@[A-Z]@', $password);
      $lowercase = preg_match('@[a-z]@', $password);
      $number    = preg_match('@[0-9]@', $password);
      $specialChars = preg_match('@[^\w]@', $password);
      return (!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 8) ? false : true;
    }
    if (empty($firstName) || empty($lastName) || empty($middleName) || empty($email) || empty($username) || empty($password) || empty($dateOfBirth)) {
      array_push($errors, "Please fill up required fields.");
    }
    if (!validateStringInput($firstName) || !validateStringInput($firstName) || !validateStringInput($middleName)) {
      array_push($errors, "Numeric characters are not allowed.");
    }
    if (!validateEmail($email)) {
      array_push($errors, "Invalid Email Address.");
    }
    if (!validatePassword($password)) {
      array_push($errors, "Password should be at least 8 characters in length and should include at least one upper case letter, one number, and one special character.");
    }
    if ($age < 0) {
      array_push($errors, "Invalid Date. You haven't been born yet. Come back on " . $dateOfBirth . "");
    }
    if (empty($_FILES["fileToUpload"]["tmp_name"])) {
      array_push($errors, "Please upload an profile image.");
    } else {
      $target_dir = "uploads/";
      $fileName = basename(rand(1, 99999) . ' - ' . $_FILES["fileToUpload"]["name"]);
      $target_file = $target_dir . $fileName;
      $uploadOk = 1;
      $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
      // Check if image file is a actual image or fake image

      $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
      if ($check !== false) {
        // array_push($errors, "File is an image - " . $check["mime"] . ".");
        $uploadOk = 1;
      } else {
        array_push($errors, "File is not an image.");
        $uploadOk = 0;
      }

      // Check if file already exists
      if (file_exists($target_file)) {
        array_push($errors, "Sorry, file already exists.");
        $uploadOk = 0;
      }

      // Check file size
      if ($_FILES["fileToUpload"]["size"] > 500000) {
        array_push($errors, "Sorry, your file is too large.");
        $uploadOk = 0;
      }

      // Allow certain file formats
      if (
        $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif"
      ) {
        array_push($errors, "Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
        $uploadOk = 0;
      }

      // Check if $uploadOk is set to 0 by an error
      if ($uploadOk == 0) {
        array_push($errors, "Sorry, your file was not uploaded.");

        // if everything is ok, try to upload file
      } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
          $image = htmlspecialchars($fileName);
        } else {
          array_push($errors, "Sorry, there was an error uploading your file.");
        }
      }
    }


    $data = array(
      "firstName" => $firstName,
      "middleName" => $middleName,
      "lastName" => $lastName,
      "email" => $email,
      "username" => $username,
      "dateOfBirth" => $dateOfBirth,
      "age" => $age,
      "image" => $image ?? '',
    );

    if ($this->checkUserDuplicate($username)) {
      array_push($errors, $this->checkUserDuplicate($username));
    }
    $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
    $stmt = $this->con->prepare("INSERT INTO users(`firstName`, `middleName`, `lastName`, `email`, `username`, `dateOfBirth`, `image`, `password`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
    $stmt->bind_param('ssssssss', $firstName, $middleName, $lastName, $email, $username, $dateOfBirth, $image, $hashedPassword);
    if (count($errors) > 0) {

      return ["errors" => $errors, "data" => $data];
    } else {
      $stmt->execute();
      return ["success" => "User created successfully.", "data" => []];
    }
  }

  function checkUserDuplicate($username)
  {
    // Prepare our SQL, preparing the SQL statement will prevent SQL injection.
    if ($stmt = $this->con->prepare('SELECT username FROM users WHERE username = ?')) {
      // Bind parameters (s = string, i = int, b = blob, etc), in our case the username is a string so we use "s"
      $stmt->bind_param('s', $username);
      $stmt->execute();
      // Store the result so we can check if the account exists in the database.
      $stmt->store_result();

      if ($stmt->num_rows > 0) {
        $stmt->bind_result($username);
        $stmt->fetch();
        return "Oops! Looks like the username " . $username . " is already taken.";
      }
    }
  }

  public function logout()
  {
    session_destroy();
    header('Location: 3-5-login.php');
  }
}
