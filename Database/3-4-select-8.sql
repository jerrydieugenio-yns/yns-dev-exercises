SELECT first_name, middle_name, last_name, 
case when MAX(hire_date IS NULL) = 0 
THEN max(hire_date) END AS hire_date 
FROM employees;