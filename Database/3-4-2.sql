INSERT INTO 
`employees`(first_name, last_name, middle_name, birth_date, department_id, hire_date, boss_id) 
VALUES
('Manabu', 'Yamazaki', NULL, '1976-03-15', 1, NULL, NULL),
('Tomohiko', 'Takasago', NULL, '1974-05-24', 3, '2014-04-01', 1),
('Yuta', 'Kawakami', NULL, '1990-08-13', 4, '2014-04-01', 1),
('Shogo', 'Kubota', NULL, '1985-01-31', 4, '2014-12-01', 1),
('Lorraine', 'San Jose', 'P.', '1983-10-11', 2, '2015-03-10', 1),
('Haille', 'Dela Cruz', 'A.', '1990-11-12', 3, '2015-02-15', 2),
('Godfrey', 'Sarmenta', 'L.', '1993-09-13', 4, '2015-01-01', 1),
('Alex', 'Amistad', 'F.', '1988-04-14', 4, '2015-04-10', 1),
('Hideshi', 'Ogoshi', NULL, '1983-07-15', 4, '2014-06-01', 1),
('Kim', '', '', '1977-10-16', 5, '2015-08-06', 1);

INSERT INTO 
`departments`(name) 
VALUES
('Executive'),
('Admin'),
('Sales'),
('Development'),
('Design'),
('Marketing');

INSERT INTO 
`positions`(name) 
VALUES
('CEO'),
('CTO'),
('CFO'),
('Manager'),
('Staff');

INSERT INTO 
`employee_positions`(employee_id, position_id) 
VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 4),
(3, 5),
(4, 5),
(5, 5),
(6, 5),
(7, 5),
(8, 5),
(9, 5),
(10, 5);

