<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>1-9. User Information CSV List - Result</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <style>
    body {
      height: 100%;
      width: 100vw;
      overflow-x: hidden;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    .container {
      width: 80%;
      margin: 0 auto;
      padding: 20px 40px;
      border: 1px solid #000;
      border-radius: 10px;
      background-color: #1a1a1a;
      color: #fff;
      box-shadow: 3px 3px 5px 6px #ccc;
    }

    .container h1 {
      color: yellow;
    }

    table {
      width: 100%;
      background: rgba(255, 255, 255, 0.05);
      box-shadow: 0 8px 32px 0 rgba(31, 38, 135, 0.37);
      backdrop-filter: blur(9.0px);
      -webkit-backdrop-filter: blur(9.0px);
      border-radius: 2px;
      border: 1px solid rgba(255, 255, 255, 0.18);
    }

    td,
    th {
      border: 1px solid #999;
      padding: 0.5rem;
    }
  </style>
</head>

<body>

  <div class="container">
    <h1>User Information List</h1>
    <table>
      <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Middle Name</th>
        <th>Email</th>
        <th>Date of Birth</th>
        <th>Age</th>
        <th>Profile Image</th>
      </tr>
      <?php
      $filename = "file-v2.csv";
      if (file_exists($filename)) {
        $f = fopen($filename, "r");
        while (($line = fgetcsv($f)) !== false) {
          echo "<tr>";
          echo  "<td>" . $line[0] . "</td>";
          echo  "<td>" . $line[1] . "</td>";
          echo  "<td>" . $line[2] . "</td>";
          echo  "<td>" . $line[3] . "</td>";
          echo  "<td>" . $line[4] . "</td>";
          echo  "<td>" . $line[5] . "</td>";
          echo  "<td><img src='uploads/" . $line[6] . "' alt='Profile Image.' width='80px' height='80px'/></td>";
          echo "</tr>\n";
        }
        fclose($f);
      }
      ?>

    </table>

  </div>
</body>

</html>