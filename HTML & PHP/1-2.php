<?php
// Declaration
$firstNumber = $_GET['firstNumber'] ?? '';
$secondNumber = $_GET['secondNumber'] ?? '';
?>


<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>1-2. Basic Arithmetic</title>
	<style>
		body {
			height: 100%;
			width: 100vw;
			overflow-x: hidden;
			display: flex;
			align-items: center;
			justify-content: center;
		}

		.container {
			width: 40%;
			margin: 50px auto;
			padding: 20px 40px;
			border: 1px solid #000;
			border-radius: 10px;
			background-color: #1a1a1a;
			color: #fff;
			box-shadow: 3px 3px 5px 6px #ccc;
			display: flex;
			flex-direction: column;
			justify-content: center;
		}

		.container div {
			display: flex;
			flex-direction: column;
			justify-content: center;
		}

		.container h1 {
			color: rgba(238, 238, 0, 1);
		}

		input[type=number] {
			padding: 10px 15px;
			margin: 10px auto;
			width: 80%;
		}

		input::-webkit-outer-spin-button,
		input::-webkit-inner-spin-button {
			-webkit-appearance: none;
			margin: 0;
		}

		label {
			width: 80%;
		}

		.button-container {
			display: flex;
			flex-direction: row;
			justify-content: start;
		}

		input[type=submit] {
			padding: 10px 0;
			margin: 5px auto;
			width: 86%;
			border: none;
			box-shadow: 2px 1px 1px #ccc;
		}

		input[type=submit]:hover {
			background-color: rgba(238, 238, 0, 1);
		}
	</style>
</head>

<body>
	<div class="container">
		<h1>Basic Arithmetic</h1>
		<form action="1-2.php">
			<label>First Number</label>
			<div>
				<input type="number" name="firstNumber" required value="<?= $firstNumber ?>" />
			</div>
			<label>Second Number</label>
			<div>
				<input type="number" name="secondNumber" required value="<?= $secondNumber ?>" />
			</div>

			<label>Select an Operator:</label>
			<div class="button-container">
				<input type="submit" name="add" value="Add" />
				<input type="submit" name="subtract" value="Subtract" />
				<input type="submit" name="multiply" value="Multiply" />
				<input type="submit" name="divide" value="Divide" />
			</div>
		</form>


		<?php
		// Operation
		$result = 0;
		if (isset($_GET['add'])) {
			$result = $firstNumber + $secondNumber;
		} else if (isset($_GET['subtract'])) {
			$result = $firstNumber - $secondNumber;
		} else if (isset($_GET['multiply'])) {
			$result = $firstNumber * $secondNumber;
		} else if (isset($_GET['divide'])) {
			$result = $firstNumber / $secondNumber;
		}
		?>
		<h2>
			Result:
			<?= $result; ?>
		</h2>
	</div>
</body>

</html>