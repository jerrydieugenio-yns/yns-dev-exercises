<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>1-8. User Information CSV - Result</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />

  <style>
    body {
      height: 100%;
      width: 100vw;
      overflow-x: hidden;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    .container {
      width: 40%;
      margin: 0 auto;
      padding: 20px 40px;
      border: 1px solid #000;
      border-radius: 10px;
      background-color: #1a1a1a;
      color: #fff;
      box-shadow: 3px 3px 5px 6px #ccc;
      display: flex;
      flex-direction: column;
      justify-content: center;
    }

    .container div {
      display: flex;
      flex-direction: column;
      justify-content: center;
    }

    .container h1 {
      color: rgba(238, 238, 0, 1);
    }

    input[type=text],
    input[type=date],
    input[type=email] {
      padding: 10px 15px;
      margin: 10px auto;
      width: 80%;
    }

    label {
      margin: 0 40px;
      width: 80%;
    }

    .button-container {
      padding: 10px 20px;
      margin: 10px auto;
      width: 50%;
    }

    input[type=submit] {
      padding: 10px 20px;
      margin: 10px auto;
      width: 80%;
      border: none;
      box-shadow: 2px 1px 1px #ccc;
    }

    input[type=submit]:hover {
      background-color: rgba(238, 238, 0, 1);
    }

    .errors {
      background-color: rgba(255, 51, 51, .7);
      padding: 20px;
      margin: 10px 35px;
      border: 1px solid #FF3333;
      font-size: 14px;
      display: flex;
      flex-direction: row;
      justify-content: space-between;
    }
  </style>
</head>

<body>
  <div class="container">
    <h1>User Information</h1>


    <?php


    function fetchErrors()
    {
      $icon = '<i class="fas fa-exclamation-triangle"></i>';
      if (!empty($_SESSION['error']) && ($_SESSION['error']) !== null) {
        $errors = $_SESSION['error'];
        echo '<div class="errors">';
        foreach ($errors as $error) {
          echo $icon . $error;
        }


        echo '</div>';
      }
    }

    function fetchValuesBack($field)
    {
      if (isset($_SESSION[$field])) {
        echo $_SESSION[$field];
        unset($_SESSION[$field]);
      }
    }
    fetchErrors();



    ?>


    <form action="1-8_result.php" method="post">

      <label>Given Name: </label>
      <div>
        <input type="text" name="firstName" placeholder="Given name" value="<?= fetchValuesBack("firstName")  ?>" />
      </div>
      <label>Surname Name: </label>
      <div>
        <input type="text" name="lastName" placeholder="Sur name" value="<?= fetchValuesBack("lastName")  ?>" />
      </div>
      <label>Middle Name: </label>
      <div>
        <input type="text" name="middleName" placeholder="Middle name" value="<?= fetchValuesBack("middleName")  ?>" />
      </div>
      <label>Email: </label>
      <div>
        <input type="text" name="email" placeholder="Email Address" value="<?= fetchValuesBack("email")  ?>" />
      </div>
      <label>Birth date: </label>
      <div>
        <input type="date" name="dateOfBirth" value="<?= fetchValuesBack("dateOfBirth")  ?>" />
      </div>
      <div class="button-container">
        <input type="submit" name="submit">
      </div>
    </form>
  </div>
</body>

</html>

<?php
session_destroy();
?>