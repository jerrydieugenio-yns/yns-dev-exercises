<?php
session_start();
if ($_POST['submit']) {
  $firstName = $_POST['firstName'];
  $lastName = $_POST['lastName'];
  $middleName = $_POST['middleName'];
  $email = $_POST['email'];
  $errors = array();
  $fullName = $lastName . ', ' . $firstName . ' ' . $middleName;
  $dateOfBirth = $_POST['dateOfBirth'];
  $age = intval(date('Y-m-d')) - intval($dateOfBirth);
  $_SESSION['firstName'] = $firstName;
  $_SESSION['lastName'] = $lastName;
  $_SESSION['middleName'] = $middleName;
  $_SESSION['email'] = $email;
  $_SESSION['dateOfBirth'] = $dateOfBirth;

  function validateStringInput($input)
  {
    return preg_match("/^[a-zA-Z\s]+$/", $input);
  }
  function validateEmail($email)
  {
    $pattern = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i";
    return preg_match($pattern, $email);
  }
  if (empty($firstName) || empty($lastName) || empty($middleName) || empty($email) || empty($dateOfBirth)) {
    array_push($errors, "Please fill up required fields.");
  }
  if (!validateStringInput($firstName) || !validateStringInput($firstName) || !validateStringInput($middleName)) {
    array_push($errors, "Numeric characters are not allowed.");
  }
  if (!validateEmail($email)) {
    array_push($errors, "Invalid Email Address.");
  }
  if ($age < 0) {
    array_push($errors, "Invalid Date. You haven't been born yet. Come back on " . $dateOfBirth . "");
  }

  if ($errors) {
    $_SESSION['error'] = $errors;
    header("Location: 1-8.php");
  } else {
    $data = array($firstName, $middleName, $lastName, $email, $dateOfBirth, $age);
    $file = fopen('file-v1.csv', 'a');
    fputcsv($file, $data);
    fclose($file);
  }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>1-8. User Information CSV - Result</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <style>
    body {
      height: 100vh;
      width: 100vw;
      overflow: hidden;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    .container {
      width: 40%;
      margin: 0 auto;
      padding: 20px 40px;
      border: 1px solid #000;
      border-radius: 10px;
      background-color: #1a1a1a;
      color: #fff;
      box-shadow: 3px 3px 5px 6px #ccc;
    }

    .container h1 {
      color: yellow;
    }

    .errors {
      background-color: rgba(255, 51, 51, .7);
      padding: 20px;
      border: 1px solid #FF3333;
      font-size: 20px;
    }
  </style>
</head>

<body>
  <div class="container">
    <h1>User Information</h1>
    <p>Name: <?= $fullName ?></p>
    <p>Email: <?= $email ?></p>
    <p>Birthday: <?= $dateOfBirth ?></p>
    <p>Age: <?= $age ?></p>
  </div>
</body>

</html>