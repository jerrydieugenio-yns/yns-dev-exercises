<?php
session_start();
require '1-13-api.php';
if (isset($_SESSION['user'])) {
  header('Location: 1-13-index.php');
}
$users = new Users();
if (isset($_POST['login'])) {
  $authStatus = $users->authenticate($_POST);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>1-13. Authentication</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <style>
    body {
      height: 100vh;
      overflow-x: hidden;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    .container {
      width: 40%;
      margin: 0 auto;
      padding: 50px;
      border: 1px solid #000;
      border-radius: 10px;
      background-color: #1a1a1a;
      color: #fff;
      box-shadow: 3px 3px 5px 6px #ccc;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }

    .errors {
      background-color: rgba(255, 51, 51, .7);
      padding: 10px;
      margin: 10px;
      border: 1px solid #FF3333;
      width: 58%;
      font-size: 14px;
      display: flex;
      flex-direction: column;
    }

    .errors>i {
      font-size: 25px;
      margin-bottom: 10px;
    }

    .errors>div {
      text-align: left;
      font-size: 16px;
    }

    /* Mobile View  */
    @media only screen and (max-width: 768px) {
      .container {
        padding: 30px 120px;
      }

      .errors {
        width: 100%;
      }
    }

    .container h1 {
      color: yellow;
    }

    .container>* {
      margin: 10px;
    }

    .container input[type="password"],
    .container input[type="text"] {
      padding: 2px 15px;
      width: 300px;
      height: 40px;
      background: #e7e5e5;
      border: none;
      outline: 0.7px solid #d1d0d0;
      margin: 10px 0;
    }

    .container input[type="submit"] {
      padding: 2px 15px;
      width: 330px;
      height: 40px;

      background-color: #24a0ed;
      border: none;
      color: white;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      margin: 4px 2px 15px 2px;
      cursor: pointer;
    }

    div.center {
      text-align: center;
    }

    form div>* {
      color: #24a0ed;
      opacity: .5;
    }
  </style>
</head>

<body>

  <div class="container center">

    <h1>Login</h1>
    <?= $authStatus ?? '' ?>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
      <input type="text" name="username" id="" placeholder="Username" value="">
      <input type="password" name="password" id="" placeholder="Password">
      <input type="submit" name="login" value="Login">
      <div><a href="1-13-create.php">Don't have an account?</a></div>
    </form>
  </div>
  </div>
</body>

</html>