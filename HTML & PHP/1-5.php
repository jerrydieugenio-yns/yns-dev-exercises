<?php
// Declaration
$date = $_GET['date'] ?? '';
?>




<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>1-5. Input Date</title>
	<style>
		body {
			height: 100vh;
			width: 100vw;
			overflow: hidden;
			display: flex;
			align-items: center;
			justify-content: center;
		}

		.container {
			width: 40%;
			margin: 0 auto;
			padding: 20px 40px;
			border: 1px solid #000;
			border-radius: 10px;
			background-color: #1a1a1a;
			color: #fff;
			box-shadow: 3px 3px 5px 6px #ccc;
			display: flex;
			flex-direction: column;
			justify-content: center;
		}

		.container div {
			display: flex;
			flex-direction: column;
			justify-content: center;
		}

		.container h1 {
			color: rgba(238, 238, 0, 1);
		}

		input[type=text],
		input[type=date] {
			padding: 10px 15px;
			margin: 10px auto;
			width: 80%;
		}

		label {
			margin: 0 40px;
			width: 80%;
		}

		.button-container {
			padding: 10px 20px;
			margin: 10px auto;
			width: 100%;
		}

		input[type=submit] {
			padding: 10px 20px;
			margin: 10px auto;
			width: 86%;
			border: none;
			box-shadow: 2px 1px 1px #ccc;
		}

		input[type=submit]:hover {
			background-color: rgba(238, 238, 0, 1);
		}
	</style>
</head>

<body>
	<div class="container">
		<h1>Input Date Exercise </h1>
		<form action="1-5.php">
			<div>
				<label>Date:</label>
				<input type="date" name="date" required value="<?php echo $date; ?>" placeholder="MM/DD/YYYY" />
			</div>
			<div>
				<input type="submit" value="Get Date" />
			</div>
			<div>
				<p>Results:</p>
				<?php
				// Operation
				function getDateInfo($date)
				{
					for ($i = 1; $i <= 3; $i++) {
						echo date('Y-m-d', strtotime('+' . $i . ' day', strtotime($date))) . ' ' . date('l', strtotime('+' . $i . ' day', strtotime($date))) . ' <br />';
					}
				}
				//Initiate Function
				$result = getDateInfo($date);

				?>
			</div>
		</form>

	</div>
</body>

</html>