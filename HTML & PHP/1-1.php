<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>1-1. Hello World</title>
  <style>
    body {
      height: 100%;
      width: 100vw;
      overflow-x: hidden;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    .container {
      width: 40%;
      margin: 50px auto;
      padding: 20px 40px;
      border: 1px solid #000;
      border-radius: 10px;
      background-color: #1a1a1a;
      color: #fff;
      box-shadow: 3px 3px 5px 6px #ccc;
      display: flex;
      flex-direction: column;
      justify-content: center;
      text-align: center;
    }

    .container h1 {
      color: rgba(238, 238, 0, 1);
      font-size: 80px;
      position: relative;
      animation-name: run;
      animation-duration: 4s;
    }

    @keyframes run {
      0% {
        color: red;
        left: 0px;
        top: 0px;
      }

      25% {
        color: yellow;
        left: 200px;
        top: 0px;
      }

      50% {
        color: blue;
        left: 200px;
        top: 200px;
      }

      75% {
        color: green;
        left: 0px;
        top: 200px;
      }

      100% {
        color: red;
        left: 0px;
        top: 0px;
      }
    }
  </style>
</head>

<body>
  <div class="container">
    <h1>
      <?= "Hello World" ?>
    </h1>
  </div>
</body>

</html>