<?php
session_start();
if ($_POST['submit']) {
  $firstName = $_POST['firstName'];
  $lastName = $_POST['lastName'];
  $middleName = $_POST['middleName'];
  $email = $_POST['email'];
  $errors = array();
  $fullName = $lastName . ', ' . $firstName . ' ' . $middleName;
  $dateOfBirth = $_POST['dateOfBirth'];
  $age = intval(date('Y-m-d')) - intval($dateOfBirth);
  $_SESSION['firstName'] = $firstName;
  $_SESSION['lastName'] = $lastName;
  $_SESSION['middleName'] = $middleName;
  $_SESSION['email'] = $email;
  $_SESSION['dateOfBirth'] = $dateOfBirth;


  function validateStringInput($input)
  {
    return preg_match("/^[a-zA-Z\s]+$/", $input);
  }
  function validateEmail($email)
  {
    $pattern = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i";
    return preg_match($pattern, $email);
  }
  if (empty($firstName) || empty($lastName) || empty($middleName) || empty($email) || empty($dateOfBirth)) {
    array_push($errors, "Please fill up required fields.");
  }
  if (!validateStringInput($firstName) || !validateStringInput($lastName) || !validateStringInput($middleName)) {
    array_push($errors, "Numeric characters are not allowed.");
  }
  if (!validateEmail($email)) {
    array_push($errors, "Invalid Email Address.");
  }
  if ($age < 0) {
    array_push($errors, "Invalid Date. You haven't been born yet. Come back on " . $dateOfBirth . "");
  }

  if (empty($_FILES["fileToUpload"]["tmp_name"])) {
    array_push($errors, "Please upload an profile image.");
  } else {
    $target_dir = "uploads/";
    $fileName = basename(rand(1, 99999) . ' - ' . $_FILES["fileToUpload"]["name"]);
    $target_file = $target_dir . $fileName;
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
    // Check if image file is a actual image or fake image

    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if ($check !== false) {
      // array_push($errors, "File is an image - " . $check["mime"] . ".");
      $uploadOk = 1;
    } else {
      array_push($errors, "File is not an image.");
      $uploadOk = 0;
    }

    // Check if file already exists
    if (file_exists($target_file)) {
      array_push($errors, "Sorry, file already exists.");
      $uploadOk = 0;
    }

    // Check file size
    if ($_FILES["fileToUpload"]["size"] > 500000) {
      array_push($errors, "Sorry, your file is too large.");
      $uploadOk = 0;
    }

    // Allow certain file formats
    if (
      $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
      && $imageFileType != "gif"
    ) {
      array_push($errors, "Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
      $uploadOk = 0;
    }

    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
      array_push($errors, "Sorry, your file was not uploaded.");

      // if everything is ok, try to upload file
    } else {
      if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        $image = htmlspecialchars($fileName);
      } else {
        array_push($errors, "Sorry, there was an error uploading your file.");
      }
    }
  }

  if ($errors) {
    $_SESSION['error'] = $errors;
    header("Location: 1-10.php");
  } else {
    $data = array($firstName, $middleName, $lastName, $email, $dateOfBirth, $age, $image);
    $file = fopen('file-v2.csv', 'a');
    fputcsv($file, $data);
    fclose($file);
  }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>1-10. Upload Image - Result</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <style>
    body {
      height: 100vh;
      width: 100vw;
      overflow: hidden;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    .container {
      width: 40%;
      margin: 0 auto;
      padding: 20px 40px;
      border: 1px solid #000;
      border-radius: 10px;
      background-color: #1a1a1a;
      color: #fff;
      box-shadow: 3px 3px 5px 6px #ccc;
    }

    .container h1 {
      color: yellow;
    }

    .errors {
      background-color: rgba(255, 51, 51, .7);
      padding: 20px;
      border: 1px solid #FF3333;
      font-size: 20px;
    }
  </style>
</head>

<body>
  <div class="container">
    <h1>User Information</h1>
    <p>Name: <?= $fullName ?></p>
    <p>Email: <?= $email ?></p>
    <p>Birthday: <?= $dateOfBirth ?></p>
    <p>Age: <?= $age ?></p>
    <p>Profile Image:</p>
    <p>
      <img src="uploads/<?= $image ?>" alt="" width="200px" height="200px">
    </p>
  </div>
</body>

</html>