<?php
class Users
{
  /*-----------------*
  This supplies the table in the index
  *-----------------*/
  public function index()
  {
    $filename = "file-final.csv";
    if (file_exists($filename)) {
    }
    return $this->csvToArray($filename, ',');
  }
  /*-----------------*
  Converts CSV data to Array, which to be sent to database
  Note: this is used by multiple function.
  *-----------------*/
  public function csvToArray($filename = '', $delimiter = ',')
  {
    if (!file_exists($filename) || !is_readable($filename))
      return FALSE;

    $data = array();
    if (($handle = fopen($filename, 'r')) !== FALSE) {
      while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {

        $data[] =  $row;
      }
      fclose($handle);
    }
    return $data;
  }
  /*-----------------*
  Handles authentication and it's validation.
  *-----------------*/
  public function authenticate($post)
  {
    $errors = array();
    $username = $_POST["username"];
    $password = $_POST["password"];
    $success = 0;

    // Check if username is empty
    if (empty($username)) {
      array_push($errors, "Please enter username.");
    }
    // Check if password is empty
    if (empty($password)) {
      array_push($errors, "Please enter your password.");
    }

    $filename = "file-final.csv";
    if (file_exists($filename)) {
      $data = $this->csvToArray($filename, ',');
      foreach ($data as $value) {
        if ($value[4] === $username && $value[8] === $password) {
          $success = 1;
          break;
        }
      }
    }

    if ($success == 1) {
      $_SESSION['user'] = $username;
      header('Location: 1-13-index.php');
    }
    if ($success == 0) {
      return "Invalid Credentials.";
    }
  }
  /*-----------------*
  Create new users & validation
  *-----------------*/
  public function create()
  {
    $firstName = $_POST['firstName'];
    $lastName = $_POST['lastName'];
    $middleName = $_POST['middleName'];
    $email = $_POST['email'];
    $username = $_POST['username'];
    $password = $_POST['password'];
    $errors = array();
    $dateOfBirth = $_POST['dateOfBirth'];
    $age = intval(date('Y-m-d')) - intval($dateOfBirth);

    function validateStringInput($input)
    {
      return preg_match("/^[a-zA-Z\s]+$/", $input);
    }
    function validateEmail($email)
    {
      $pattern = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i";
      return preg_match($pattern, $email);
    }
    function validatePassword($password)
    {
      $uppercase = preg_match('@[A-Z]@', $password);
      $lowercase = preg_match('@[a-z]@', $password);
      $number    = preg_match('@[0-9]@', $password);
      $specialChars = preg_match('@[^\w]@', $password);
      return (!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 8) ? false : true;
    }
    if (empty($firstName) || empty($lastName) || empty($middleName) || empty($email) || empty($username) || empty($password) || empty($dateOfBirth)) {
      array_push($errors, "Please fill up required fields.");
    }
    if (!validateStringInput($firstName) || !validateStringInput($firstName) || !validateStringInput($middleName)) {
      array_push($errors, "Numeric characters are not allowed.");
    }
    if (!validateEmail($email)) {
      array_push($errors, "Invalid Email Address.");
    }
    if (!validatePassword($password)) {
      array_push($errors, "Password should be at least 8 characters in length and should include at least one upper case letter, one number, and one special character.");
    }
    if ($age < 0) {
      array_push($errors, "Invalid Date. You haven't been born yet. Come back on " . $dateOfBirth . "");
    }
    if (empty($_FILES["fileToUpload"]["tmp_name"])) {
      array_push($errors, "Please upload an profile image.");
    } else {
      $target_dir = "uploads/";
      $fileName = basename(rand(1, 99999) . ' - ' . $_FILES["fileToUpload"]["name"]);
      $target_file = $target_dir . $fileName;
      $uploadOk = 1;
      $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
      // Check if image file is a actual image or fake image

      $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
      if ($check !== false) {
        // array_push($errors, "File is an image - " . $check["mime"] . ".");
        $uploadOk = 1;
      } else {
        array_push($errors, "File is not an image.");
        $uploadOk = 0;
      }

      // Check if file already exists
      if (file_exists($target_file)) {
        array_push($errors, "Sorry, file already exists.");
        $uploadOk = 0;
      }

      // Check file size
      if ($_FILES["fileToUpload"]["size"] > 500000) {
        array_push($errors, "Sorry, your file is too large.");
        $uploadOk = 0;
      }

      // Allow certain file formats
      if (
        $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif"
      ) {
        array_push($errors, "Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
        $uploadOk = 0;
      }

      // Check if $uploadOk is set to 0 by an error
      if ($uploadOk == 0) {
        array_push($errors, "Sorry, your file was not uploaded.");

        // if everything is ok, try to upload file
      } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
          $image = htmlspecialchars($fileName);
        } else {
          array_push($errors, "Sorry, there was an error uploading your file.");
        }
      }
    }

    $data = array(
      "firstName" => $firstName,
      "middleName" => $middleName,
      "lastName" => $lastName,
      "email" => $email,
      "username" => $username,
      "dateOfBirth" => $dateOfBirth,
      "age" => $age,
      "image" => $image ?? '',
      "password" => $password,
    );

    if (count($errors) > 0) {
      return ["errors" => $errors, "data" => $data];
    } else {
      $file = fopen('file-final.csv', 'a');
      fputcsv($file, $data);
      fclose($file);
      return ["success" => "User created successfully.", "data" => []];
    }
  }
  /*-----------------*
    Logout
  *-----------------*/
  public function logout()
  {
    session_destroy();
    header('Location: 1-13-login.php');
  }
}
