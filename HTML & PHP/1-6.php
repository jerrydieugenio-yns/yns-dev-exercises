<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>1-6. User Information - Result</title>
	<style>
		body {
			height: 100vh;
			width: 100vw;
			overflow: hidden;
			display: flex;
			align-items: center;
			justify-content: center;
		}

		.container {
			width: 40%;
			margin: 0 auto;
			padding: 20px 40px;
			border: 1px solid #000;
			border-radius: 10px;
			background-color: #1a1a1a;
			color: #fff;
			box-shadow: 3px 3px 5px 6px #ccc;
			display: flex;
			flex-direction: column;
			justify-content: center;
		}

		.container div {
			display: flex;
			flex-direction: column;
			justify-content: center;
		}

		.container h1 {
			color: rgba(238, 238, 0, 1);
		}

		input[type=text],
		input[type=date] {
			padding: 10px 15px;
			margin: 10px auto;
			width: 80%;
		}

		label {
			margin: 0 40px;
			width: 80%;
		}

		.button-container {
			padding: 10px 20px;
			margin: 10px auto;
			width: 50%;
		}

		input[type=submit] {
			padding: 10px 20px;
			margin: 10px auto;
			width: 80%;
			border: none;
			box-shadow: 2px 1px 1px #ccc;
		}

		input[type=submit]:hover {
			background-color: rgba(238, 238, 0, 1);
		}
	</style>
</head>

<body>
	<div class="container">
		<h1>User Information</h1>
		<form action="1-6_result.php" method="post">

			<label>Given Name: </label>
			<div>
				<input type="text" name="firstName" placeholder="Given name" required />
			</div>
			<label>Surname Name: </label>
			<div>
				<input type="text" name="lastName" placeholder="Sur name" required />
			</div>
			<label>Middle Name: </label>
			<div>
				<input type="text" name="middleName" placeholder="Middle name" required />
			</div>
			<label>Birth date: </label>
			<div>
				<input type="date" name="dateOfBirth" required />
			</div>
			<div class="button-container">
				<input type="submit" name="submit">
			</div>
		</form>
	</div>
</body>

</html>