<?php

if ($_POST['submit']) {
  $firstName = $_POST['firstName'];
  $lastName = $_POST['lastName'];
  $middleName = $_POST['middleName'];
  $fullName = $lastName . ', ' . $firstName . ' ' . $middleName;
  $dateOfBirth = $_POST['dateOfBirth'];
  $age = intval(date('Y-m-d')) - intval($dateOfBirth);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>1-6. User Information - Result</title>
  <style>
    body {
      height: 100vh;
      width: 100vw;
      overflow: hidden;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    .container {
      width: 40%;
      margin: 0 auto;
      padding: 20px 40px;
      border: 1px solid #000;
      border-radius: 10px;
      background-color: #1a1a1a;
      color: #fff;
      box-shadow: 3px 3px 5px 6px #ccc;
    }

    .container h1 {
      color: yellow;
    }
  </style>
</head>

<body>
  <div class="container">
    <h1>User Information</h1>
    <p>Name: <?= $fullName ?></p>
    <p>Birthday: <?= $dateOfBirth ?></p>
    <p>Age: <?= $age ?></p>
  </div>
</body>

</html>