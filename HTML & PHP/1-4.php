<?php
// Declaration
$number = $_GET['number'] ?? '';
?>


<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>1-4. FizzBuzz</title>
	<style>
		body {
			height: 100%;
			width: 100vw;
			overflow-x: hidden;
			display: flex;
			align-items: center;
			justify-content: center;
		}

		.container {
			width: 40%;
			margin: 50px auto;
			padding: 20px 40px;
			border: 1px solid #000;
			border-radius: 10px;
			background-color: #1a1a1a;
			color: #fff;
			box-shadow: 3px 3px 5px 6px #ccc;
			display: flex;
			flex-direction: column;
			justify-content: center;
		}

		.container div {
			display: flex;
			flex-direction: column;
			justify-content: center;
		}

		.container h1 {
			color: rgba(238, 238, 0, 1);
		}

		input[type=number] {
			padding: 10px 15px;
			margin: 10px auto;
			width: 80%;
		}

		input::-webkit-outer-spin-button,
		input::-webkit-inner-spin-button {
			-webkit-appearance: none;
			margin: 0;
		}

		label {

			width: 80%;
		}

		.button-container {
			padding: 10px 20px;
			margin: 10px auto;
			width: 100%;
		}

		input[type=submit] {
			padding: 10px 20px;
			margin: 10px auto;
			width: 86%;
			border: none;
			box-shadow: 2px 1px 1px #ccc;
		}

		input[type=submit]:hover {
			background-color: rgba(238, 238, 0, 1);
		}
	</style>
</head>

<body>
	<div class="container">
		<h1>FizzBuzz</h1>
		<form action="1-4.php">
			<label>Enter maximum number: </label>
			<input type="number" name="number" required value="<?php echo $number; ?>" placeholder="9999" />
			<input type="submit" value="FizzBuzz" />
		</form>

		<?php
		// Operation
		function fizzBuzz($number)
		{
			for ($i = 1; $i <= $number; $i++) {
				if ($i % 15 == 0) {
					echo 'FizzBuzz<br>';
				} elseif ($i % 3 == 0) {
					echo 'Fizz<br>';
				} elseif ($i % 5 == 0) {
					echo 'Buzz<br>';
				} else {
					echo $i . '<br>';
				}
			}
		}
		//Initiate Function
		$result = fizzBuzz($number);

		?>


		<?php
		// Declaration
		$date = $_GET['date'] ?? '';
		?>

	</div>
</body>

</html>