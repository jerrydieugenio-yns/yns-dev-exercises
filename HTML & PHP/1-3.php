<?php
// Declaration
$firstNumber = $_GET['firstNumber'] ?? '';
$secondNumber = $_GET['secondNumber'] ?? '';
?>



<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>1-3. Greatest Common Divisor</title>
	<style>
		body {
			height: 100%;
			width: 100vw;
			overflow-x: hidden;
			display: flex;
			align-items: center;
			justify-content: center;
		}

		.container {
			width: 40%;
			margin: 50px auto;
			padding: 20px 40px;
			border: 1px solid #000;
			border-radius: 10px;
			background-color: #1a1a1a;
			color: #fff;
			box-shadow: 3px 3px 5px 6px #ccc;
			display: flex;
			flex-direction: column;
			justify-content: center;
		}

		.container div {
			display: flex;
			flex-direction: column;
			justify-content: center;
		}

		.container h1 {
			color: rgba(238, 238, 0, 1);
		}

		input[type=number] {
			padding: 10px 15px;
			margin: 10px auto;
			width: 80%;
		}

		input::-webkit-outer-spin-button,
		input::-webkit-inner-spin-button {
			-webkit-appearance: none;
			margin: 0;
		}

		label {
			width: 80%;
		}

		.button-container {
			padding: 10px 20px;
			margin: 10px auto;
			width: 50%;
		}

		input[type=submit] {
			padding: 10px 20px;
			margin: 10px auto;
			width: 80%;
			border: none;
			box-shadow: 2px 1px 1px #ccc;
		}

		input[type=submit]:hover {
			background-color: rgba(238, 238, 0, 1);
		}
	</style>
</head>

<body>
	<div class="container">
		<h1>Greatest Common Divisor </h1>
		<form action="1-3.php">
			<div>
				<label for="">First Number: </label>
				<input type="number" name="firstNumber" required value="<?php echo $firstNumber; ?>" />
			</div>
			<div>
				<label for="">Second Number: </label>
				<input type="number" name="secondNumber" required value="<?php echo $secondNumber; ?>" />
			</div>
			<div class="button-container">
				<input type="submit" name="gcd" value="Get Greatest Common Divisor" />
			</div>
		</form>

		<?php
		// Operation
		function gcd($firstNumber, $secondNumber)
		{
			return ($firstNumber % $secondNumber) ? gcd($secondNumber, $firstNumber % $secondNumber) : $secondNumber;
		}
		//Initiate Function
		$result = ($firstNumber && $secondNumber) ? gcd($firstNumber, $secondNumber) : '';
		?>

		<h2>Result: <?= $result ?></h2>

	</div>
</body>

</html>