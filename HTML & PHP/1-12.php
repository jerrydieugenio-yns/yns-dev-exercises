<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>1-12. Pagination</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <style>
    body {
      height: 100%;
      width: 100vw;
      overflow-x: hidden;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    .container {
      width: 80%;
      margin: 0 auto;
      padding: 20px 40px;
      border: 1px solid #000;
      border-radius: 10px;
      background-color: #1a1a1a;
      color: #fff;
      box-shadow: 3px 3px 5px 6px #ccc;
    }

    .container h1 {
      color: yellow;
    }

    table {
      width: 100%;
      background: rgba(255, 255, 255, 0.05);
      box-shadow: 0 8px 32px 0 rgba(31, 38, 135, 0.37);
      backdrop-filter: blur(9.0px);
      -webkit-backdrop-filter: blur(9.0px);
      border-radius: 5px;
    }

    td,
    th {
      border: 1px solid #999;
      padding: 0.5rem;
      text-align: center;
    }

    td img {
      border-radius: 50px;
      width: 70px;
      height: 70px;
      object-fit: cover;
    }



    ul.pagination {
      display: inline-block;
      padding: 0;
      margin: 0;
    }

    ul.pagination li {
      display: inline;
    }

    ul.pagination li a {
      color: white;
      float: left;
      padding: 8px 16px;
      text-decoration: none;
      transition: background-color .3s;
      background: rgba(255, 255, 255, 0.08);
      box-shadow: 0 8px 32px 0 rgba(31, 38, 135, 0.37);
      backdrop-filter: blur(9.0px);
      -webkit-backdrop-filter: blur(9.0px);
      border-radius: 2px;
      border: 1px solid rgba(255, 255, 255, 0.18);
    }

    ul.pagination li a.active {
      background-color: yellow;
      color: #000;
      border: 1px solid #4CAF50;
      opacity: .9;

    }

    ul.pagination li a:hover:not(.active) {
      background-color: #ddd;
      color: #000;

    }

    div.center {
      text-align: center;
    }
  </style>
</head>

<body>

  <div class="container">
    <h1>Paginated User Information List</h1>

    <?php
    $filename = "file-v2.csv";
    if (file_exists($filename)) {
      function csvToArray($filename = '', $delimiter = ',')
      {
        if (!file_exists($filename) || !is_readable($filename))
          return FALSE;

        $data = array();
        if (($handle = fopen($filename, 'r')) !== FALSE) {
          while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {

            $data[] =  $row;
          }
          fclose($handle);
        }
        return $data;
      }

      //Count total number of rows
      $total = count(file($filename, FILE_SKIP_EMPTY_LINES));
      // Limit per Page
      $limit = 10;
      // How many pages will there be
      $pages = ceil($total / $limit);
      // What page are we currently on?
      $page = min($pages, filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, array(
        'options' => array(
          'default'   => 1,
          'min_range' => 1,
        ),
      )));
      // Calculate the offset for the query
      $offset = ($page - 1)  * $limit;
      // Some information to display to the user
      $start = $offset + 1;
      $end = min(($offset + $limit), $total);

      $data = csvToArray($filename, ',');

      echo '<table>
        <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Middle Name</th>
          <th>Email</th>
          <th>Date of Birth</th>
          <th>Age</th>
          <th>Profile Image</th>
        </tr>';
      //Display Records
      for ($i = $start - 1; $i < $end; $i++) {
        echo "<tr>";
        foreach ($data[$i] as $key => $value) {

          if ($key == 6) {
            echo "<td><img src='uploads/" . $value . "' /></td>";
          } else {
            echo "<td>" . $value . "</td>";
          }
        }
        echo "</tr>";
      }
      echo '</table>';

      echo '<div class="center">';
      // Previous Page
      $prevlink = (true) ? '<a href="?page=1" title="First page">&laquo;</a> <a href="?page=' . ($page - 1) . '" title="Previous page">&lsaquo;</a>' : '<span class="disabled">&laquo;</span> <span class="disabled">&lsaquo;</span>';
      // Next Page
      $nextlink = (true) ? '<a href="?page=' . ($page + 1) . '" title="Next page">&rsaquo;</a> <a href="?page=' . $pages . '" title="Last page">&raquo;</a>' : '<span class="disabled">&rsaquo;</span> <span class="disabled">&raquo;</span>';
      // Display Paging
      echo '<div id="paging"><p> Page ', $page, ' of ', $pages, ' - Displaying ', $start, '-', $end, ' of ', $total, ' results  </p></div>';

      // Display Complete Pagination
      echo '<ul class="pagination">';
      echo '<li>' . $prevlink . '</li>';
      for ($counter = 1; $counter <= $pages; $counter++) {
        if ($counter == $page) {
          echo "<li><a class='active'>$counter</a></li>";
        } else {
          echo "<li><a href='?page=$counter'>$counter</a></li>";
        }
      }
      echo '<li>' . $nextlink . '</li>';
      echo '</ul>';
      echo '</div>';
    }
    ?>
  </div>
</body>

</html>