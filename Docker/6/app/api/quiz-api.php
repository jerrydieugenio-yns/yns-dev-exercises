<?php
class Quiz
{
    private $server = "mysql-server";
    private $username = "root";
    private $password = "secret";
    private $database = "yns_quiz";
    public $con;

    public function __construct()
    {
        $this->con = new mysqli($this->server, $this->username, $this->password, $this->database);
        if (mysqli_connect_error()) {
            trigger_error("Failed to connect to Database: " . mysqli_connect_error());
        } else {
            return $this->con;
        }
    }
}

class Questions extends Quiz
{
    public function data()
    {
        $stmt = $this->con->prepare("SELECT id, question FROM `questions` ORDER BY rand() LIMIT 10");
        $stmt->execute();
        $result = $stmt->get_result();
        $data = array();
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
        }
        return json_encode($data);
    }
}

class Answers extends Quiz
{
    public function data()
    {
        $stmt = $this->con->prepare("SELECT * FROM `answers`");
        $stmt->execute();
        $result = $stmt->get_result();
        $data = array();
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
        }
        return json_encode($data);
    }
}
