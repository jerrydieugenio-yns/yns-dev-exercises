<?php
session_start();
class DatabaseConnection
{
    private $server = "mysql-server";
    private $username = "root";
    private $password = "secret";
    private $database = "yns_quiz";
    public $con;

    public function __construct()
    {
        $this->con = new mysqli($this->server, $this->username, $this->password, $this->database);
        if (mysqli_connect_error()) {
            trigger_error("Failed to connect to Database: " . mysqli_connect_error());
        } else {
            return $this->con;
        }
    }
}

class Login extends DatabaseConnection
{
    public function auth()
    {
        $errors = array();
        $email = $_POST["email"];

        if ($stmt = $this->con->prepare('SELECT id, password FROM users WHERE username = ?')) {
            $stmt->bind_param('s', $email);
            $stmt->execute();
            $stmt->store_result();

            if ($stmt->num_rows > 0) {
                $stmt->bind_result($id, $password);
                $stmt->fetch();
                if (password_verify($_POST['password'], $password)) {
                    session_regenerate_id();
                    $_SESSION['loggedin'] = TRUE;
                    $_SESSION['user'] = $email;
                    $_SESSION['id'] = $id;

                    header('Location: quiz.php');
                } else {
                    // Incorrect password
                    array_push($errors, "Incorrect username and/or password!");
                }
            } else {
                // Incorrect username
                array_push($errors, "Incorrect username and/or password!");
            }


            $stmt->close();
            $data = array(
                "email" => $email,
            );
            if (count($errors) > 0) {
                return ["errors" => $errors, "data" => $data];
            }
        }
    }
}

class Register extends DatabaseConnection
{
    public function save()
    {
        $name = $this->con->real_escape_string($_POST['name']);
        $email = $this->con->real_escape_string($_POST['email']);
        $password = $this->con->real_escape_string($_POST['password']);
        $errors = [];
        function validateStringInput($input)
        {
            return preg_match("/^[a-zA-Z\s]+$/", $input);
        }
        function validateEmail($email)
        {
            $pattern = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i";
            return preg_match($pattern, $email);
        }
        function validatePassword($password)
        {
            $uppercase = preg_match('@[A-Z]@', $password);
            $lowercase = preg_match('@[a-z]@', $password);
            $number    = preg_match('@[0-9]@', $password);
            $specialChars = preg_match('@[^\w]@', $password);
            return (!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 8) ? false : true;
        }
        if (empty($name) || empty($email) || empty($password)) {
            array_push($errors, "Please fill up required fields.");
        }
        if (!validateEmail($email)) {
            array_push($errors, "Invalid Email Address.");
        }
        if (!validatePassword($password)) {
            array_push($errors, "Password should be at least 8 characters in length and should include at least one upper case letter, one number, and one special character.");
        }

        $data = array(
            "name" => $name,
            "email" => $email,
            "password" => $password,
        );
        if ($this->checkUserDuplicate($email)) {
            array_push($errors, $this->checkUserDuplicate($email));
        }
        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
        $stmt = $this->con->prepare("INSERT INTO users(`name`, `username`, `password`) VALUES (?, ?, ?)");
        $stmt->bind_param('sss', $name, $email, $hashedPassword);

        if (count($errors) > 0) {
            return ["errors" => $errors, "data" => $data];
        } else {
            $stmt->execute();
            return ["success" => "User created successfully.", "data" => []];
        }
    }

    function checkUserDuplicate($email)
    {
        if ($stmt = $this->con->prepare('SELECT email FROM users WHERE email = ?')) {
            $stmt->bind_param('s', $email);
            $stmt->execute();
            $stmt->store_result();

            if ($stmt->num_rows > 0) {
                $stmt->bind_result($email);
                $stmt->fetch();
                return "Oops! Looks like the username " . $email . " is already taken.";
            }
        }
    }
}
