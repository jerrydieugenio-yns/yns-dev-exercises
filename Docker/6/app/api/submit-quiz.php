<?php
session_start();
$server = "mysql-server";
$username = "root";
$password = "secret";
$database = "yns_quiz";

$con = new mysqli($server, $username, $password, $database);
if (mysqli_connect_error()) {
    trigger_error("Failed to connect to Database: " . mysqli_connect_error());
}

if (isset($_POST['final_score'])) {
    $score = $con->real_escape_string($_POST['final_score']);
    $user_id = $_SESSION['id'];

    $stmt = $con->prepare("INSERT INTO results(`user_id`, `score`, `date_taken`) VALUES (?, ?, now())");
    $stmt->bind_param('ii', $user_id, $score);
    $stmt->execute();
}
