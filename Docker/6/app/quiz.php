<?php
session_start();

include_once 'api/quiz-api.php';

if (!isset($_SESSION['loggedin'])) {
    header("Location: login.php");
}




$questions = new Questions();
$questions_list = $questions->data();
$answers = new Answers();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quiz</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="assets/css/main.css">

</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <a class="navbar-brand" href="/">YNS</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" href="/">Dashboard</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="quiz.php">Quiz</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="calendar.php">Calendar</a>
                    </li>

                    <?php
                    if (isset($_SESSION['loggedin'])) {
                    ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Jerry Di Eugenio
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="api/logout.php">Logout</a></li>
                            </ul>
                        </li>
                    <?php
                    } else {
                    ?>
                        <li class="nav-item">
                            <a class="nav-link" href="login.html">Sign In</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="register.html">Sign Up</a>
                        </li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container bg-light my-5 p-5 text-center" id="start_box">
        <i class="far fa-edit quiz-icon-main my-2"></i>
        <h1 class="my-2">Test Your Limits - YNS Quiz Application</h1>
        <button class="btn btn-primary bg-red my-4" id="startBtn">Start</button>
    </div>

    <div class="container bg-light my-5 p-5 text-start d-none" id="question_box">
        <div class="time-container bg-red">
            <i class="far fa-clock"></i>
            <span id="time"></span>
        </div>
        <div id="score"></div>

        <h1 class="my-2 question question_text" id="question"></h1>
        <p class="lead" id="description"></p>
        <div class="text-center" id="answers">
            <button class="btn btn-primary bg-red my-4 choice">Choice A</button>
        </div>
    </div>

    <div class="container bg-light my-5 p-5 text-center d-none " id="result_box">
        <i class="far fa-check-circle quiz-icon-main my-2 text-green"></i>
        <h1 class="my-2">Congratulations, you passed!</h1>
        <p class="lead">
            Your Score: <span class="fw-bold" id="final_score">100% (10 points)</span>
        </p>
        <p class="lead">
            Passing Score: <span class="fw-bold" id="passing_score">80% (8 points)</span>
        </p>
        <button class="btn btn-primary btn-review-quiz my-4" id="reviewQuizBtn">Review Quiz</button>
    </div>


    <div class="container bg-light my-5 p-5 text-center d-none " id="result_summary">
        <i class="far fa-check-circle quiz-icon-main my-2 text-green"></i>
        <h1 class="my-2">Quiz Summary</h1>
        <p class="lead" id="time_spent"></p>
        <p class="lead result_summary_content text-start"></p>

    </div>


    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>


    <script>
        function startTimer(duration) {
            var start = Date.now(),
                diff,
                minutes,
                seconds;

            function timer() {
                diff = duration - (((Date.now() - start) / 1000) | 0);

                minutes = (diff / 60) | 0;
                seconds = (diff % 60) | 0;

                minutes = minutes < 10 ? "0" + minutes : minutes;
                seconds = seconds < 10 ? "0" + seconds : seconds;

                document.querySelector('#time').textContent = minutes + ":" + seconds;

                if (diff <= 0) {
                    start = Date.now() + 1000;
                }
            };
            timer();
            let interval = setInterval(timer, 1000);
        }

        // Fetch Random Question & Answers from PHP
        const questions = <?= $questions->data() ?>;
        let answers = <?= $answers->data() ?>;


        const startBtn = document.querySelector("#startBtn");
        const startQuiz = () => {
            // Time Limit
            var time = 60 * 5;
            // Start the Timer
            startTimer(time);


            startBtn.parentElement.classList.add('d-none');
            document.querySelector("#start_box").classList.add('d-none');
            document.querySelector("#question_box").classList.remove('d-none');
            console.log('Quiz Started');

            // This means, the algorithm will get the first question in the array
            // Note: The questions were already randomized when fetched.
            let count = 0;

            showQuestions(count);
            // When the user clicks an answer. It automatically proceed to the next question.
            document.querySelector(".choice").addEventListener("click", () => {
                showQuestions(count);
            });

        };

        // For tracking of questions asked.
        let questions_asked = [];
        // List the quiz results including the total quiz results, questions asked, the user's answer and the correct answer.
        let final_results = [];

        const result_box = document.querySelector("#result_box");

        // Show questions function
        const showQuestions = (index) => {
            const question_text = document.querySelector(".question_text");
            if (questions_asked.length < 10) {
                showAnswers(questions[index]);
                let question_tag = '<span>' + questions[index].question + '</span>';
                question_text.innerHTML = question_tag;
            } else {
                // If the user is already asked 10 questions
                console.log('Out of Questions, Quiz is done!');
                question_text.parentElement.classList.add('d-none');
                result_box.classList.remove('d-none');
            }
        };

        // Show answers function
        const showAnswers = (question) => {
            const answers_container = document.getElementById("answers");

            console.log("Current Question: ", question);
            // This gives all the answers that is associated with that question.
            const results = answers.filter((answer) => {
                return answer.question_id == question.id;
            });
            // This returns the correct answer
            const correct_answer = answers.filter((answer) => {
                return answer.question_id == question.id && answer.is_correct === 1;
            });

            // This generates choices buttons and set the process for the answer
            results.forEach((value, key) => {
                let choiceBtn = document.createElement('button');
                choiceBtn.className = "btn btn-primary bg-red my-4 mx-2 choice";
                choiceBtn.innerText = value.answer;
                choiceBtn.onclick = function() {
                    processAnswer(question, question.id, question.question, value.answer, value.is_correct, correct_answer[0].answer);
                };
                answers_container.replaceChild(choiceBtn, answers_container.childNodes[key]);
            });
        };
        let score = 0;
        const processAnswer = (current_question, question_id, question, answer, is_correct, correct_answer) => {

            final_results.push({
                question_id: question_id,
                question: question,
                answer: answer,
                is_correct: is_correct,
                correct_answer: correct_answer
            });
            // Inserts the current question to the Question_Asked Array and Removes it From the Questions Pool
            // This solves the probabilty of questions to be asked again
            questions_asked.push(current_question);
            questions.splice(questions.indexOf(current_question), 1);

            console.log("Questions Asked: ", questions_asked);
            console.log("Questions Left: ", questions);

            // If the chosen answer is correct. Will add 1 to Score
            if (is_correct == 1) {
                score++;
            }
            document.getElementById("score").innerHTML = score;
            document.getElementById("final_score").innerHTML = ((score / 10) * 100) + `% (${score} points out of ${final_results.length})`;

            //console.log("Current Score: ", score);
            //console.log("Your answer is: " + answer);

            //This will show another question after the answer is processed.
            showQuestions(0);

        };


        const showFinalResult = () => {
            // This provide all the items that is answered correctly.
            const initial_score = final_results.filter((answer) => {
                return answer.answer == answer.correct_answer;
            });
            //This tallies the score and generate the final score of the user.
            let final_score = initial_score.length;

            // Used ajax to save the quiz result to the database.
            $.ajax({
                url: "api/submit-quiz.php",
                method: "post",
                data: {
                    final_score,
                },
                success: function(data) {
                    console.log("Successfuly Finished Quiz and Score has been Recorded.")
                },
                error: function() {
                    console.log('Error. There must be something wrong.');
                }
            });
            // The following lines of codes are for showing the summary of the Quiz Taken ( Includes questions, answers and the final score.)
            const result_summary = document.querySelector("#result_summary");
            const result_summary_content = document.querySelector(".result_summary_content");
            result_summary.classList.remove('d-none');
            result_box.classList.add('d-none');

            let score = document.createElement('h5');
            score.className = "m-2 h5";
            score.innerText = "Total Score: " + ((final_score / 10) * 100) + `% (${final_score} points out of ${final_results.length})`;;
            result_summary_content.appendChild(score);

            // This loops everything (The questions asked, the answer of the user and the correct answer)
            // This is helpful in order to know where the user have gone wrong in the test and will definitely give them good learning experience.
            final_results.forEach((value, key) => {
                let question = document.createElement('p');
                question.className = "m-2 lead";
                question.innerText = "Question " + (key + 1) + ": " + value.question;
                let answer = document.createElement('p');
                answer.className = "m-2 lead";

                answer.innerText = "Your Answer: " + value.answer;

                let correct_answer = document.createElement('p');
                correct_answer.className = "m-2 my-4 lead border-bottom";
                if (value.is_correct == 0) {

                    answer.classList.add("bg-danger");
                    correct_answer.classList.add("bg-success");
                } else {
                    answer.classList.add("bg-success");
                }

                correct_answer.innerText = "Correct Answer: " + value.correct_answer;

                result_summary_content.appendChild(question);
                result_summary_content.appendChild(answer);
                result_summary_content.appendChild(correct_answer);
            });

            // In the final results and summary, time spent in the exam is also included.
            // Quiz takers can also determine how fast they took the test.
            document.querySelector('#time_spent').innerHTML = "Time Spent: " + document.querySelector('#time').innerText;
        };

        let reviewQuizBtn = document.getElementById("reviewQuizBtn");
        reviewQuizBtn.addEventListener("click", showFinalResult);
        startBtn.addEventListener("click", startQuiz);
    </script>

</body>

</html>