-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 09, 2021 at 09:48 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yns_quiz`
--
CREATE DATABASE IF NOT EXISTS `yns_quiz` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `yns_quiz`;

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
CREATE TABLE IF NOT EXISTS `answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `answer` varchar(50) NOT NULL,
  `is_correct` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `question_id`, `answer`, `is_correct`) VALUES
(1, 1, 'Jerry', 1),
(2, 1, 'Tommy', 0),
(3, 1, 'Mimi', 0),
(4, 2, 'They use their feet', 0),
(5, 2, 'They can\'t', 0),
(6, 2, 'They  fly with their wings.', 1),
(7, 3, '1st', 1),
(8, 3, '2nd', 0),
(9, 3, '3rd', 0),
(10, 4, '2', 0),
(11, 4, '1', 0),
(12, 4, 'All of them', 1),
(13, 5, 'THE ANSWER.', 1),
(14, 5, 'An elephant.', 0),
(15, 5, 'The data given is insufficient.', 0),
(16, 6, '40.5', 0),
(17, 6, '70', 1),
(18, 6, 'I know this is a trick question, so NONE. Ha!', 0),
(19, 7, 'The red clock.', 1),
(20, 7, 'The blue clock.', 0),
(21, 7, 'Neither.', 0),
(22, 8, '8', 1),
(23, 8, '9', 0),
(24, 8, '25', 0),
(25, 9, 'Yes', 1),
(26, 9, 'No', 0),
(27, 9, 'Depends on the leaf.', 0),
(28, 10, '3', 0),
(29, 10, '42', 0),
(30, 10, '45', 1);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `description`) VALUES
(1, 'What is my name?', 'State full name.'),
(2, 'How do birds fly?', 'Explain.'),
(3, 'You\'re 3rd place right now in a race.', 'What place are you in when you pass the person in 2nd place?'),
(4, 'How many 0.5cm slices of bread can you cut from a whole bread that\'s 25cm long?', ''),
(5, 'The answer is really big. ', 'What is it?'),
(6, 'Divide 30 by half and add ten.', 'What\'s your answer?'),
(7, 'There are two clocks of different colors: The red clock is broken and doesn\'t run at all, but the blue clock loses one second every 24 hours.', 'Which clock is more accurate? '),
(8, 'A farmer has 17 sheep, all of them but 8 die.', 'How many sheep are still standing? '),
(9, 'If a leaf falls to the ground in a forest and no one hears it, does it make a sound?', 'Does it, does it?'),
(10, 'There are 45 apples in your basket. You take three apples out of the basket.', 'How many apples are left?');

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

DROP TABLE IF EXISTS `results`;
CREATE TABLE IF NOT EXISTS `results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `date_taken` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `results`
--



-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
